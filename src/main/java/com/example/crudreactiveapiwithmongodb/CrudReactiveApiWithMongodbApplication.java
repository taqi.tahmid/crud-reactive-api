package com.example.crudreactiveapiwithmongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrudReactiveApiWithMongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(CrudReactiveApiWithMongodbApplication.class, args);
	}

}
