package com.example.crudreactiveapiwithmongodb.repository;

import com.example.crudreactiveapiwithmongodb.entity.Product;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

public interface ProductRepository extends ReactiveCrudRepository<Product,Integer> {
    @Query("INSERT INTO public.product\n" +
            "(id, \"name\", qty, price)\n" +
            "VALUES(3, :name , :qty ,:price);\n")
    Mono<Product>save(String name,int qty, int price);
    @Query("SELECT * from public.product pd where pd.name= :name and pd.qty= :qty and price= :price")
    Mono<Product>findProduct(String name,int qty, int price);

    @Query("SELECT * from public.product pd where pd.id= :id")
    Mono<Product>findById(int id);
}
