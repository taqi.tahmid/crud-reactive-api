package com.example.crudreactiveapiwithmongodb.service;

import com.example.crudreactiveapiwithmongodb.dto.ProductDto;
import com.example.crudreactiveapiwithmongodb.entity.Product;
import com.example.crudreactiveapiwithmongodb.repository.ProductRepository;
import com.example.crudreactiveapiwithmongodb.utils.AppUtils;
import com.example.crudreactiveapiwithmongodb.web.Exception.AlreadyExistsException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.FileNotFoundException;
import java.util.List;
@Slf4j
@Service
public class ProductService {

    @Autowired
    private ProductRepository repository;

    public Mono<List<ProductDto>> getProducts(){

       return repository.findAll().map(AppUtils::entityToDto)
               .collectList();

    }

    public Mono<ProductDto> getProductById(String id){
        log.info("received userId: {}",id);
        int id_;
        try{
            //here trying something new
            id_ = Integer.parseInt(id);
        }
        catch (Exception e){

            //comments from main
            return Mono.empty();
        }
        //this isfrom main
        //let's see if it works
        //we will find out soon
        return repository.findById(id_)
                        .map(AppUtils::entityToDto)
                        .doOnSuccess(pd->log.info("received product dto: {}",pd))
                        .switchIfEmpty(Mono.error(new FileNotFoundException()));


    }
    public  Mono<ProductDto> updateProduct (Mono<ProductDto> productDtoMono) {
        return productDtoMono.map(AppUtils:: dtoToEntity)
                .flatMap(product -> repository.findProduct(product.getName(), product.getQty(), product.getPrice()))
                .flatMap(product->{
                    //changes in update
                    //from main
                    if(product.getId()== null){
                        log.info("found null");
                        return repository.save(product.getName(),product.getQty(),product.getPrice())
                                .map(AppUtils::entityToDto);
                    }
                    else {
                        return Mono.error(new AlreadyExistsException("User already exists"));
                    }
                });
    }

    public Mono<ProductDto> saveProduct (Mono<ProductDto> productDtoMono){

        return productDtoMono.map(AppUtils:: dtoToEntity)
                .flatMap(product -> repository.findProduct(product.getName(), product.getQty(), product.getPrice()))
                .flatMap(product->{
                    if(product.getId()== null){
                        log.info("found null");
                       return repository.save(product.getName(),product.getQty(),product.getPrice())
                               .map(AppUtils::entityToDto);
                    }
                    else {
                       return Mono.error(new AlreadyExistsException("User already exists"));
                    }
                    });
    }


}
