package com.example.crudreactiveapiwithmongodb.web.handlers;

import com.example.crudreactiveapiwithmongodb.dto.ProductDto;
import com.example.crudreactiveapiwithmongodb.service.ProductService;
import com.example.crudreactiveapiwithmongodb.utils.AppUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Slf4j
@Component
public class ProductHandler {

    private final ProductService productService;

    public ProductHandler(ProductService service) {
        this.productService = service;
    }

    public Mono<ServerResponse> getProductHandler (ServerRequest serverRequest){
        return   productService.getProducts()
                .flatMap(list -> list.isEmpty() ?
                        ServerResponse.status(HttpStatus.NO_CONTENT).bodyValue("No content found") :
                        ServerResponse.ok()
                                .contentType(MediaType.APPLICATION_JSON)
                                .bodyValue(list)
                );
    }

    public Mono<ServerResponse> saveProductHandler (ServerRequest serverRequest){
        Mono<ProductDto>productDtoMono=serverRequest.bodyToMono(ProductDto.class);
        return  productService.saveProduct(productDtoMono)
                .flatMap(product-> ServerResponse.ok().bodyValue(product))
                .doOnSuccess(e->log.info("saved ",e))
                .doOnError(e->log.info(e.getMessage()))
                .onErrorResume(e->ServerResponse.status(HttpStatus.BAD_REQUEST).bodyValue(e.getMessage()));
    }

    public Mono<ServerResponse> getProductByIdHandler (ServerRequest serverRequest){
        return  productService.getProductById(serverRequest.pathVariable("id"))
                .flatMap(pd->ServerResponse.ok()
                        .contentType(MediaType.APPLICATION_JSON)
                        .bodyValue(pd))
                .switchIfEmpty(ServerResponse.status(HttpStatus.NOT_FOUND).bodyValue("No content found"))
                .doOnSuccess(e->log.info("dto : {}",e))
                .onErrorResume(e->ServerResponse.status(HttpStatus.NOT_FOUND).bodyValue("No content found"));
    }


}
