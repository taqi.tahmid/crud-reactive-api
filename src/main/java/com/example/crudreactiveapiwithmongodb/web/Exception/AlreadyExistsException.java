package com.example.crudreactiveapiwithmongodb.web.Exception;

public class AlreadyExistsException extends RuntimeException{
    public AlreadyExistsException(String message){
        super(message);
    }
}
