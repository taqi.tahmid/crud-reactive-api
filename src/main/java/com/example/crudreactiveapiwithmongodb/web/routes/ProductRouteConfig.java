package com.example.crudreactiveapiwithmongodb.web.routes;

import com.example.crudreactiveapiwithmongodb.web.handlers.ProductHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import org.springframework.web.server.ServerWebInputException;

import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;


@Slf4j
@Configuration
public class ProductRouteConfig {

    @Bean
    public RouterFunction<ServerResponse> ProductRoutes(ProductHandler productHandler) {

        return route()
                .path(
                        RouteNames.PRODUCT_BASE_URL,
                        builder -> builder
                                .nest(RequestPredicates.accept(APPLICATION_JSON), nestedBuilder ->
                                        nestedBuilder
                                                .GET(RouteNames.GET_PRODUCTS, productHandler::getProductHandler)
                                )
                                .nest(RequestPredicates.accept(APPLICATION_JSON), nestedBuilder ->
                                        nestedBuilder
                                                .POST(RouteNames.SAVE_PRODUCT, productHandler::saveProductHandler)
                                )
                                .nest(RequestPredicates.accept(APPLICATION_JSON), nestedBuilder ->
                                        nestedBuilder
                                                .GET(RouteNames.GET_PRODUCT_BY_ID, productHandler::getProductByIdHandler)
                                                .before(this::validatePathVariable)
                                )


                )

                .build();



    }

    public ServerRequest validatePathVariable(ServerRequest serverRequest){
        checkIfPathVariableExists(serverRequest);
        return serverRequest;
    }

    public void checkIfPathVariableExists(ServerRequest serverRequest){
       Optional<String> productId= Optional.of(serverRequest.pathVariable("id"));
        if (productId.isEmpty() || productId.get().isBlank() || !productId.isPresent()) {
            log.error("error occurred while checking pathvariable :{}",productId.get());
            throw new ServerWebInputException("Product is required");
        }

    }

}
